var WebElements = require('./WebElements.json');
var logger = require('./log');

describe('Test Log in', function() {
	
	it('login using valid credentials', function() {

		var EC = protractor.ExpectedConditions;
		var elm = element(by.css(WebElements.locators.loginPage.usernameField));

		

		browser.get(WebElements.loginPageUrll);

		logger.log('info','waiting for element ' + elm);
		browser.wait(EC.presenceOf(elm), 30000);

		element(by.css(WebElements.locators.loginPage.usernameField)).sendKeys(WebElements.userDetails.proAccount);
		logger.log('info','Entering username of ' + WebElements.userDetails.proAccount);
		console.log("testing");
		element(by.css(WebElements.locators.loginPage.passwordField)).sendKeys(WebElements.userDetails.proPassword);
		logger.log('info','Entering username of ' + WebElements.userDetails.proPassword);

		elm = element(by.css(WebElements.locators.loginPage.test));
		logger.log('info','waiting for element ' + elm);
		browser.wait(EC.presenceOf(elm), 30000);
		element(by.css("[type='submit']")).click();
		logger.log('finished');


	});

});