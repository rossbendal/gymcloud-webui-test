# README #

This README is for Automated WebUI Testing for GymCloud WebApp and Landing Page.

### What is this repository for? ###

* Automated UI testing using Protractor
* Version 1.0

### How do I get set up? ###

* Install Java/JDK
* Install Node.js
* Install NPM
* npm install -g protractor
* webdriver-manager update
* webdriver-manager start

### Configuration / Running Tests ###

* Start webdriver manager (webdriver-manager start)
* Open test directory and run by using (protractor conf.js)
* Make sure the test you want to run is declared in conf.js