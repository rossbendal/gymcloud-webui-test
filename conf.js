let SpecReporter = require('jasmine-spec-reporter').SpecReporter;
var webRep = require('jasmine-slack-reporter');
var Jasmine2HtmlReporter = require('protractor-jasmine2-html-reporter');
var SlackWebhook = require('slack-webhook');
var slack = new SlackWebhook('https://hooks.slack.com/services/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');

exports.config = {

	capabilities: {
		'browserName' : 'chrome',
	},
  	seleniumAddress: 'http://localhost:4444/wd/hub',
  	specs: ['./smoke-test/proWarmups.js'],
    suites: {
      smoke: [
        './smoke-test/loginAccountType.js',
        './smoke-test/clientFullSignup.js',
        './smoke-test/proFullSignup.js',
        './smoke-test/proExercises.js',
        './smoke-test/proWarmups.js',
        ]
    },

  	onPrepare: function () {
  		browser.ignoreSynchronization = true,
      jasmine.getEnv().addReporter(new SpecReporter({
        spec: {
          displayStacktrace: true
        }
      })),
      jasmine.getEnv().addReporter(
        new Jasmine2HtmlReporter({
          savePath: './reports',
          takeScreenshotsOnlyOnFailures: true,
        })
      );
    },

  	jasmineNodeOpts: {
  	  // Default time to wait in ms before a test fails.
  	  defaultTimeoutInterval: 100000,
      print: function() {},
	  },

    
};

