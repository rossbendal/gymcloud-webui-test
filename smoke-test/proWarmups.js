var WebElements = require('../WebElements.json');
var generate = require('project-name-generator');
var thoughts = require('thoughts');
var description = thoughts.random();
var desc = description.thought;
var EC = protractor.ExpectedConditions;
var elm;

describe('Pro WarmUps functionality', function() {

	it('should log existing PRO in dashboard', function() {
		browser.get("https://gymcloud:GCstaging@app.s.gymcloud.com/");
		browser.get("https://app.s.gymcloud.com/");
		elm = element(by.css(WebElements.locators.loginPage.usernameField));
		browser.wait(EC.presenceOf(elm), 30000);
		element(by.css(WebElements.locators.loginPage.usernameField)).sendKeys(WebElements.stagingUserDetails.proAccount);
		element(by.css(WebElements.locators.loginPage.passwordField)).sendKeys(WebElements.stagingUserDetails.proPassword);
		element(by.css(WebElements.locators.loginPage.loginButton)).click();
		
		//wait for pro-homepage element
		elm = element(by.css(WebElements.locators.proDashboard.workoutsThisWeek));
		browser.wait(EC.presenceOf(elm), 90000);
		expect(element(by.css(WebElements.locators.proDashboard.workoutsThisWeek)).isPresent()).toBe(true);
	});

	it('should redirect the user to WarmUps list when sidebar item is clicked.', function() {
		element(by.css("[data-name='warmups'")).click();
		elm = element(by.css(".gc-breadcrumbs-title a"));
		browser.wait(EC.presenceOf(elm), 30000);
		expect(element(by.css(".gc-breadcrumbs-title a")).getText()).toEqual('WARMUP TEMPLATES');
	});

	//TC-WP009
	it('should display created warmups.', function() {
		expect(element(by.css(".gc-exercises-link")).isPresent()).toBeTruthy();
	});

	//TC-WP009
	it('should display add WarmUp popup when clicked.', function() {
		element(by.css(".btn.gc-show-add-exercise-modal")).click();
		elm = element(by.css("h4.modal-title"));
		browser.wait(EC.presenceOf(elm), 30000);
		expect(element(by.css("h4.modal-title")).isPresent()).toBeTruthy();
		browser.sleep(3000);
	});

	//TC-WP010
	it('should be able to enter WarmUp Name in add popup modal window', function() {
		elm = element(by.css("[maxlength='128']"));
		browser.wait(EC.presenceOf(elm), 30000);
		element(by.css("[maxlength='128']")).sendKeys(generate().spaced);
	});

	//TC-WP010
	it('Should be able to add new WarmUp once ADD WARMUP has been clicked ', function() {
		element(by.css("[type='submit']")).click();
		elm = element(by.css(".messenger-will-hide-after"));
		browser.wait(EC.presenceOf(elm), 30000);
		expect(element(by.css(".messenger-will-hide-after")).getText()).toEqual('New item added');
	});

	//TC-WP010
	it('should redirect user to exercise overview upon successful exercise creation', function() {
		elm = element(by.css(".workout-template"));
		browser.wait(EC.presenceOf(elm), 30000);
		expect(element(by.css(".workout-template")).isPresent()).toBeTruthy();
	});

	// TC-WP011
	it('should display video popup screen when add video is clicked.', function() {
		browser.sleep(3000);
		element(by.css(".gc-add-video")).click();
		elm = element(by.css("h4.modal-title"));
		browser.wait(EC.presenceOf(elm), 30000);
		expect(element(by.css("h4.modal-title")).isPresent()).toBeTruthy();
		browser.sleep(3000);
		element(by.css(".video-search-input")).clear().sendKeys("warmups");
	});

	// TC-WP011
	it('should be able to click for YOUTUBE tab in video pop-up', function() {
		element(by.css("[data-scope='youtube']")).click();
		elm = element(by.css(".gc-video-details button"));
		browser.wait(EC.presenceOf(elm), 30000);
		expect(element(by.css(".gc-video-details button")).isPresent()).toBeTruthy();
	});

	// TC-WP011
	it('should be able to assign a searched youtube video', function() {
		elm = element(by.css(".gc-video-player"));
		browser.wait(EC.presenceOf(elm), 30000);
		element(by.css(".gc-video-details button")).click();
		expect(element(by.css(".gc-video-player")).isPresent()).toBeTruthy();
	});

	// TC-WP012
	it('should add description in newly created warmup', function() {
		element(by.css("textarea")).sendKeys(desc);
	});

	// TC-WP013
	it('should save Warmup details after clicking SAVE', function() {
		element(by.css(".save.btn")).click();
		elm = element(by.css(".gc-author-widget"));
		browser.wait(EC.presenceOf(elm), 30000);
		expect(element(by.css("region[data-name='workout_exercises_constructor']")).isPresent()).toBeTruthy();
		browser.executeScript('window.sessionStorage.clear();');
    	browser.executeScript('window.localStorage.clear();');
	});

});