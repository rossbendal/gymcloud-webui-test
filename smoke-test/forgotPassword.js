var WebElements = require('../WebElements.json');
var EC = protractor.ExpectedConditions;
var elm;

describe('Forgot password functionality', function() {
	
    it('should be able to redirect user to forgot password screen', function() {
    	browser.get(WebElements.loginPageUrl);
		elm = element(by.css(WebElements.locators.loginPage.usernameField));
		browser.wait(EC.presenceOf(elm), 30000);
		element(by.css(WebElements.locators.loginPage.forgotPasswordButton)).click();
		elm = element(by.css(WebElements.locators.forgotPasswordPage.emailField));
		browser.wait(EC.presenceOf(elm), 30000);
    });

	it('should be able to block user from entering invalid email', function() {
		element(by.css(WebElements.locators.forgotPasswordPage.emailField)).sendKeys("shouldBeInvalid");
		expect(element(by.css(WebElements.locators.forgotPasswordPage.sendbuttonDisabled)).isPresent()).toBe(true);
	});

	it('should be able to redirect user to confirmation screen once valid email is ', function() {
		element(by.css(WebElements.locators.forgotPasswordPage.emailField)).sendKeys("prodtrainer@yopmail.com");
		element(by.css(WebElements.locators.forgotPasswordPage.sendbutton)).click();
		expect(element(by.css(WebElements.locators.forgotPasswordConfirmationPage.textBody)).isPresent()).toBe(true);
	});
});