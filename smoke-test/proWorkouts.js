var WebElements = require('../WebElements.json');
var generate = require('project-name-generator');
var EC = protractor.ExpectedConditions;
var elm;

describe('Pro Workout functionalities', function() {
	
	it('should log existing PRO in dashboard', function() {
		browser.get(WebElements.loginPageUrl);
		elm = element(by.css(WebElements.locators.loginPage.usernameField));
		browser.wait(EC.presenceOf(elm), 30000);
		element(by.css(WebElements.locators.loginPage.usernameField)).sendKeys(WebElements.userDetails.proAccount);
		element(by.css(WebElements.locators.loginPage.passwordField)).sendKeys(WebElements.userDetails.proPassword);
		element(by.css(WebElements.locators.loginPage.loginButton)).click();
		
		//wait for pro-homepage element
		elm = element(by.css(WebElements.locators.proDashboard.workoutsThisWeek));
		browser.wait(EC.presenceOf(elm), 30000);
		expect(element(by.css(WebElements.locators.proDashboard.workoutsThisWeek)).isPresent()).toBe(true);
	});

	it('should redirect the user to workout list when sidebar item is clicked.', function() {
		element(by.css("[data-name='workout_templates']")).click();
		elm = element(by.css(".gc-breadcrumbs-title a"));
		browser.wait(EC.presenceOf(elm), 30000);
		expect(element(by.css(".gc-breadcrumbs-title a")).getText()).toEqual('WORKOUT TEMPLATES');
	});

	it('should display created exercises.', function() {
		expect(element(by.css(".gc-folders-entity-list")).isPresent()).toBeTruthy();
	});

	it('should display add workout popup when clicked.', function() {
		element(by.css(".gc-show-add-exercise-modal.btn-link")).click();
		elm = element(by.css("h4.modal-title"));
		browser.wait(EC.presenceOf(elm), 30000);
		expect(element(by.css("h4.modal-title")).isPresent()).toBeTruthy();
		browser.sleep(3000);
	});

	it('should create new workout when I add name and click add button', function() {
		elm = element(by.css("[maxlength='128']"));
		browser.wait(EC.presenceOf(elm), 30000);
		element(by.css("[maxlength='128']")).sendKeys(generate().spaced);
		element(by.css("[type='submit']")).click().then(function(){
			elm = element(by.css(".messenger-will-hide-after"));
			browser.wait(EC.presenceOf(elm), 30000);
			expect(element(by.css(".messenger-will-hide-after")).getText()).toEqual('New item added');
		});
	});

	it('should redirect user to workout overview upon successful workout creation', function() {
		elm = element(by.css(".workout-template"));
		browser.wait(EC.presenceOf(elm), 30000);
		expect(element(by.css(".workout-template")).isPresent()).toBeTruthy();
	});
});