var WebElements = require('../WebElements.json');
var EC = protractor.ExpectedConditions;
var elm;

describe('Check Public Marketing Pages', function() {
	
	beforeEach(function(){
		browser.get(WebElements.landingPageUrl);
	});

	it('redirect user to landing page', function() {
		// elm = element(by.css(WebElements.locators.landingPage.landingTitle));
		// browser.wait(EC.presenceOf(elm), 30000);
		expect(element(by.css(WebElements.locators.landingPage.landingTitle)).isPresent()).toBe(true);
	});

	it('clicking clients page should redirect user so client page', function() {
		element(by.css(WebElements.locators.landingPage.clientFooterLink)).click();
		// elm = element(by.css(WebElements.locators.clientsPage.clientH1));
		// browser.wait(EC.presenceOf(elm), 30000);
		expect(element(by.css(WebElements.locators.clientsPage.clientH1)).isPresent()).toBe(true);
	});

	it('clicking fit-pro page should redirect user to fit-pro page', function() {
		element(by.css(WebElements.locators.landingPage.fitProFooterLink)).click();
		// elm = element(by.css(WebElements.locators.fitproPage.fitProH1));
		// browser.wait(EC.presenceOf(elm), 30000);
		expect(element(by.css(WebElements.locators.fitproPage.fitProH1)).isPresent()).toBe(true);
	});

	it('clicking about should redirect user to about us page', function() {
		element(by.css(WebElements.locators.landingPage.aboutFooterLink)).click();
		// elm = element(by.css(WebElements.locators.aboutUsPage.aboutUsH1));
		// browser.wait(EC.presenceOf(elm), 30000);
		expect(element(by.css(WebElements.locators.aboutUsPage.aboutUsH1)).isPresent()).toBe(true);
	});

	it('clicking contact should redirect user to contact us page', function() {
		element(by.css(WebElements.locators.landingPage.contactFooterLink)).click();
		// elm = element(by.css(WebElements.locators.contactUsPage.contactUsH1));
		// browser.wait(EC.presenceOf(elm), 30000);
		expect(element(by.css(WebElements.locators.contactUsPage.contactUsH1)).isPresent()).toBe(true);
	});

	it('clicking blog should redirect user to blog page', function() {
		element(by.css(WebElements.locators.landingPage.blogHeader)).click();
		// elm = element(by.css(WebElements.locators.blogPage.blogFilter));
		// browser.element(EC.presenceOf(elm), 30000);
		expect(element(by.css(WebElements.locators.blogPage.blogFilter)).isPresent()).toBe(true);
	});

	it('clicking privacy policy should redirect user to policy page', function() {
		element(by.css(WebElements.locators.landingPage.privacyPolicyFooterLink)).click();
		// elm = element(by.css(WebElements.locators.blogPage.blogFilter));
		// browser.element(EC.presenceOf(elm), 30000);
		expect(element(by.css(WebElements.locators.privacyPolicyPage.privacySection)).isPresent()).toBe(true);
	});

	it('clicking terms should redirect user to terms page', function() {
		element(by.css(WebElements.locators.landingPage.termsFooterLink)).click();
		// elm = element(by.css(WebElements.locators.blogPage.blogFilter));
		// browser.element(EC.presenceOf(elm), 30000);
		expect(element(by.css(WebElements.locators.termsPage.termsSection)).isPresent()).toBe(true);
	});

	it('clicking agreement should redirect user to agreement page', function() {
		element(by.css(WebElements.locators.landingPage.termsFooterLink)).click();
		// elm = element(by.css(WebElements.locators.blogPage.blogFilter));
		// browser.element(EC.presenceOf(elm), 30000);
		expect(element(by.css(WebElements.locators.termsPage.termsSection)).isPresent()).toBe(true);
	});

});