var WebElements = require('../WebElements.json');
var generate = require('project-name-generator');
var thoughts = require('thoughts');
var description = thoughts.random();
var desc = description.thought;
var EC = protractor.ExpectedConditions;
var elm;

describe('Pro exercise functionalities', function() {

	it('should log existing PRO in dashboard', function() {
		browser.get("https://gymcloud:GCstaging@app.s.gymcloud.com/");
		browser.get("https://app.s.gymcloud.com/");
		elm = element(by.css(WebElements.locators.loginPage.usernameField));
		browser.wait(EC.presenceOf(elm), 30000);
		element(by.css(WebElements.locators.loginPage.usernameField)).sendKeys(WebElements.stagingUserDetails.proAccount);
		element(by.css(WebElements.locators.loginPage.passwordField)).sendKeys(WebElements.stagingUserDetails.proPassword);
		element(by.css(WebElements.locators.loginPage.loginButton)).click();
		
		//wait for pro-homepage element
		elm = element(by.css(WebElements.locators.proDashboard.workoutsThisWeek));
		browser.wait(EC.presenceOf(elm), 90000);
		expect(element(by.css(WebElements.locators.proDashboard.workoutsThisWeek)).isPresent()).toBe(true);
	});

	it('should redirect the user to exercise list when sidebar item is clicked.', function() {
		element(by.css("[data-name='exercises'")).click();
		elm = element(by.css(".gc-breadcrumbs-title a"));
		browser.wait(EC.presenceOf(elm), 30000);
		expect(element(by.css(".gc-breadcrumbs-title a")).getText()).toEqual('EXERCISES');
		
	});

	it('should display created exercises.', function() {
		expect(element(by.css(".gc-exercises-link")).isPresent()).toBeTruthy();
	});

	it('should display add exercise popup when clicked.', function() {
		element(by.css(".btn.gc-show-add-exercise-modal")).click();
		elm = element(by.css("h4.modal-title"));
		browser.wait(EC.presenceOf(elm), 30000);
		expect(element(by.css("h4.modal-title")).isPresent()).toBeTruthy();
		browser.sleep(3000);
	});

	it('should be able to enter Exercise Name in add popup modal window', function() {
		elm = element(by.css("[maxlength='128']"));
		browser.wait(EC.presenceOf(elm), 30000);
		element(by.css("[maxlength='128']")).sendKeys(generate().spaced);
	});

	it('Should be able to add new exercise once ADD EXERCISE has been clicked ', function() {
		element(by.css("[type='submit']")).click();
		elm = element(by.css(".messenger-will-hide-after"));
		browser.wait(EC.presenceOf(elm), 30000);
		expect(element(by.css(".messenger-will-hide-after")).getText()).toEqual('New item added');
	});
	
	it('should redirect user to exercise overview upon successful exercise creation', function() {
		elm = element(by.css("[placeholder='Exercise Name'][type='text']"));
		browser.wait(EC.presenceOf(elm), 30000);
		expect(element(by.css("[placeholder='Exercise Name'][type='text']")).isPresent()).toBeTruthy();
	});

	it('should display video popup screen when add video is clicked.', function() {
		browser.sleep(3000);
		element(by.css(".gc-add-video")).click();
		elm = element(by.css("h4.modal-title"));
		browser.wait(EC.presenceOf(elm), 30000);
		expect(element(by.css("h4.modal-title")).isPresent()).toBeTruthy();
		browser.sleep(3000);
		element(by.css(".video-search-input")).clear().sendKeys("exercises");
	});

	it('should be able to click for YOUTUBE tab in video pop-up', function() {
		element(by.css("[data-scope='youtube']")).click();
		elm = element(by.css(".gc-video-details button"));
		browser.wait(EC.presenceOf(elm), 30000);
		expect(element(by.css(".gc-video-details button")).isPresent()).toBeTruthy();
	});

	it('should be able to assign a searched youtube video', function() {
		elm = element(by.css(".gc-video-player"));
		browser.wait(EC.presenceOf(elm), 30000);
		element(by.css(".gc-video-details button")).click();
		expect(element(by.css(".gc-video-player")).isPresent()).toBeTruthy();
	});

	it('should add description in newly created exercise', function() {
		element(by.css(".gc-exercise-description-edit")).sendKeys(desc);
	});

	it('should save exercises details after clicking SAVE', function() {
		element(by.css(".gc-save-exercise")).click();
		elm = element(by.css(".gc-author-widget"));
		browser.wait(EC.presenceOf(elm), 30000);
		expect(element(by.css(".gc-edit-exercise")).isPresent()).toBeTruthy();
		browser.executeScript('window.sessionStorage.clear();');
    	browser.executeScript('window.localStorage.clear();');
	});
});