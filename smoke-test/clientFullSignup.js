var EC = protractor.ExpectedConditions;
var elm;
var faker = require("faker");
var firstName = faker.Name.firstName();
var lastName = faker.Name.lastName();
var email = faker.Internet.email();
var randomName = faker.Name.findName();

function randomArrayValue(max) {
		    return Math.floor(Math.random() * ((max + 1) - 0)) + 0
		};

describe('Client Registration and Assessment', function() {
	
	//pre-testing, staging site needs auth
	browser.get("https://gymcloud:GCstaging@app.s.gymcloud.com/#login");
	browser.get("https://app.s.gymcloud.com/#login");
	element(by.css("[href='#login']"));
	elm = element(by.css(".btn[data-social='fb']"));
	browser.wait(EC.presenceOf(elm), 60000);

	it('should redirect user to client registration form', function() {
		browser.get("https://app.s.gymcloud.com/#signup");
		elm = element(by.css("h1.text-center"));
		browser.wait(EC.presenceOf(elm), 60000);
		expect(element(by.css("h1.text-center")).getText()).toEqual('Create a GymCloud Account');
	});

	it('populate form with valid credentials', function() {
		element(by.css("input[name='first_name']")).sendKeys(firstName);
		element(by.css("input[name='last_name']")).sendKeys(lastName);
		element(by.css("input[name='email']")).sendKeys(email);
		element(by.css("input[name='password']")).sendKeys("gymcloud");
		element(by.css("input[name='password_confirmation']")).sendKeys("gymcloud");
		element(by.css("input[name='agreement']")).click();
	});

	it('should redirect user to Training Plan Screen when client registration submit is clicked', function() {
		element(by.css("button[type='submit']")).click();
		elm = element(by.css("div.popular_whmDR"));
		browser.wait(EC.presenceOf(elm), 60000);
		expect(element(by.css("h1 span")).getText()).toEqual('Select Training Plan');
	});

	it('should be able to pick account type', function() {
		element(by.css("[type='submit']")).click();
		elm = element(by.css(".number[type='text']"));
		browser.wait(EC.presenceOf(elm), 60000);

	});

	it('should enter payment information', function() {
		element(by.css(".number[type='text']")).sendKeys("6011000990139424"); //Discover
		element(by.css("select.exp_month")).element(by.css("[value='05']")).click();
		element(by.css("select.exp_year")).element(by.css("[value='2021']")).click();
		element(by.css("input.cvc[type='text']")).sendKeys("1234");
		element(by.css("input.full-name[type='text']")).clear().sendKeys(randomName);
		elm = element(by.css("[type='submit']"));
		browser.wait(EC.presenceOf(elm), 60000);
		browser.sleep(5000);
		
	});

	it('should redirect user to successful payment screen upon clicking Next', function() {
		element(by.css("button[type='submit']")).click();
		elm = element(by.css(".icon_6onz-.row"));
		browser.wait(EC.presenceOf(elm), 60000);
		expect(element(by.css("h1 span")).getText()).toEqual('Payment Successful!');
	});

	it('should redirect user to Assessment-Q1', function() {
		browser.sleep(5000);
		element(by.css(".gc-form-submit-button")).click();
		elm = element(by.css(".title_1Xfm-"));
		browser.wait(EC.presenceOf(elm), 60000);
		expect(element(by.css(".title_1Xfm-")).getText()).toEqual('Select Gender');
	});


	it('should contain Male and Female answers for gender screen', function() {
		element.all(by.css('label .gender_text_3PBZy')).then(function(gender){
			expect(gender[0].getText()).toEqual('Male');
			expect(gender[1].getText()).toEqual('Female');
		});
	});

	it('should select a random gender', function() {

		element.all(by.css('label .gender_text_3PBZy')).then(function(gender){
		gender[randomArrayValue(gender.length-1)].click();
		});
	});

	it('should redirect the user to Assessment-Q2', function() {
		element(by.css(".next_3Ebd9")).click();
		elm = element(by.css("[name='birthday']"));
		browser.wait(EC.presenceOf(elm), 60000);
		expect(element(by.css(".question_number_1vF87")).getText()).toEqual("Question 2");
	});

	it('should enter birthday input', function() {
		var bday = "03/02/1999";
		element(by.css("[name='birthday']")).sendKeys(bday);
		element(by.css("[name='birthday']")).sendKeys(protractor.Key.ESCAPE);

		element(by.css("[name='birthday']")).getAttribute('value').then(function(text){
			expect(text).toEqual(bday);
		});
	});

	it('should enter height-feet input', function() {
		var feet = "5";
		element(by.css("[name='height_feet']")).clear().sendKeys(feet);
		element(by.css("[name='height_feet']")).getAttribute('value').then(function(text){
			expect(text).toEqual(feet);
		});
	});

	it('should enter height-inches input', function() {
		var inches = "10";
		element(by.css("[name='height_inches']")).clear().sendKeys(inches);
		element(by.css("[name='height_inches']")).getAttribute('value').then(function(text){
			expect(text).toEqual(inches);
		});
	});

	it('should enter weight input', function() {
		var weight = "157";
		element(by.css("[name='weight']")).clear().sendKeys(weight);
		element(by.css("[name='weight']")).getAttribute('value').then(function(text){
			expect(text).toEqual(weight);
		});
	});

	it('should redirect user to Assessment-Q3', function() {
		element(by.css(".next_3Ebd9")).click();
		elm = element(by.css(".question_number_s4jWq"));
		browser.wait(EC.presenceOf(elm), 60000);
		expect(element(by.css(".question_number_s4jWq")).getText()).toEqual("Question 3");
	});

	it('should enter text for "What led you to decided to start this program now?"', function() {
		var temp = "I want to get fit by training and working hard. Also, I want to instill discipline to myself.";
		element(by.css(".question_NVyuQ [required='']")).clear().sendKeys(temp);
		element(by.css(".question_NVyuQ [required='']")).getAttribute('value').then(function(text){
			expect(text).toEqual(temp);
		});
	});

	it('should redirect user to Assessment-Q4', function() {
		element(by.css(".next_3Ebd9")).click();
		elm = element(by.css(".next_3Ebd9"));
		browser.wait(EC.presenceOf(elm), 60000);
		expect(element(by.css(".question_number_3isMs")).getText()).toEqual("Question 4");
	});

	it('should select random numbers of selection Q4', function() {
		element.all(by.css(".checkbox_3RHbv")).then(function(selection){
			for(x=0;x<randomArrayValue(8);x++){
				selection[randomArrayValue(selection.length-1)].click();
			};
		});
	});

	it('should redirect user to Assessment-Q5', function() {
		element(by.css(".next_3Ebd9")).click();
		elm = element(by.css(".title_lHKah"));
		browser.wait(EC.presenceOf(elm), 60000);
		expect(element(by.css(".title_lHKah")).getText()).toEqual("Select your primary Fitness Goal");
	});

	it('should select second option on selection Q5', function() {
		element(by.css(".radio_3Sdnv")).click();
	});

	it('should redirect user to Assessment-Q6', function() {
		element(by.css(".next_3Ebd9")).click();
		elm = element(by.css(".next_3Ebd9.disabled"));
		browser.wait(EC.presenceOf(elm), 60000);
		expect(element(by.css(".title_3AkIE")).getText()).toEqual('Choose the aspect of "Improved Health and Well-Being" that is most important to you.');
	});

	it('should select second option on selection Q6', function() {
		element(by.css(".radio_1jT4A")).click();
	});

	it('should redirect user to Assessment-Q7', function() {
		element(by.css(".next_3Ebd9")).click();
		elm = element(by.css(".title_wGfGw"));
		browser.wait(EC.presenceOf(elm), 60000);
		expect(element(by.css(".title_wGfGw")).getText()).toEqual('What aspect of your life do you believe is preventing you from reaching your fitness goals?');
	});

	it('should enter text for Q7 text area', function() {
		var temp = "I want to get fit by training and working hard. Also, I want to instill discipline to myself.";
		element(by.css("textarea")).clear().sendKeys(temp);
		element(by.css("textarea")).getAttribute('value').then(function(text){
			expect(text).toEqual(temp);
		});
	});

	it('should redirect user to confirmation screen', function() {
		element(by.css(".next_3Ebd9")).click();
		elm = element(by.css(".question_RqEd7"));
		browser.wait(EC.presenceOf(elm), 60000);
		expect(element(by.css(".question_RqEd7")).isPresent()).toBeTruthy();
	});

	it('should redirect user to congratulatory screen', function() {
		element(by.css(".next_3Ebd9.gc-btn-primary-outline")).click();
		elm = element(by.css(".title_JzLod"));
		browser.wait(EC.presenceOf(elm), 60000);
		expect(element(by.css(".title_JzLod")).getText()).toEqual('You have completed the written assessment.');
	});

	it('should display START NOW button', function() {
		expect(element(by.css(".btn_2pHdk")).isPresent()).toBeTruthy();
		browser.executeScript('window.sessionStorage.clear();');
    	browser.executeScript('window.localStorage.clear();');
	});


});