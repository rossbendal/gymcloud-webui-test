var WebElements = require('../WebElements.json');
var EC = protractor.ExpectedConditions;
var elm;

describe('Test Login Functionality using:', function() {

	beforeEach(function(){
		browser.get(WebElements.stagingLoginPageUrl);
		browser.get("https://app.s.gymcloud.com/#login");
	});

	it('login pro using valid credentials', function() {
		elm = element(by.css(WebElements.locators.loginPage.usernameField));
		browser.wait(EC.presenceOf(elm), 30000);
		element(by.css(WebElements.locators.loginPage.usernameField)).sendKeys(WebElements.stagingUserDetails.proAccount);
		element(by.css(WebElements.locators.loginPage.passwordField)).sendKeys(WebElements.stagingUserDetails.proPassword);
		element(by.css(WebElements.locators.loginPage.loginButton)).click();
		
		//wait for pro-homepage element
		elm = element(by.css(WebElements.locators.proDashboard.workoutsThisWeek));
		browser.wait(EC.presenceOf(elm), 30000);
		expect(element(by.css(WebElements.locators.proDashboard.workoutsThisWeek)).isPresent()).toBe(true);

		//log out
		element(by.css(WebElements.locators.proDashboard.accountMenu)).click();
		elm = element(by.css(WebElements.locators.proDashboard.menuLogout));
		browser.wait(EC.presenceOf(elm), 30000);
		element(by.css(WebElements.locators.proDashboard.menuLogout)).click();
	});

	it('login client using valid credentials', function() {
		elm = element(by.css(WebElements.locators.loginPage.usernameField));
		browser.wait(EC.presenceOf(elm), 30000);
		element(by.css(WebElements.locators.loginPage.usernameField)).sendKeys(WebElements.stagingUserDetails.clientAccount);
		element(by.css(WebElements.locators.loginPage.passwordField)).sendKeys(WebElements.stagingUserDetails.clientPassword);
		element(by.css(WebElements.locators.loginPage.loginButton)).click();

		//wait for client-homepage element
		elm = element(by.css(WebElements.locators.clientDashboard.upcomingWorkouts));
		browser.wait(EC.presenceOf(elm), 30000);
		expect(element(by.css(WebElements.locators.clientDashboard.upcomingWorkouts)).isPresent()).toBe(true);

		//log out
		element(by.css(WebElements.locators.proDashboard.accountMenu)).click();
		elm = element(by.css(WebElements.locators.proDashboard.menuLogout));
		browser.wait(EC.presenceOf(elm), 30000);
		element(by.css(WebElements.locators.proDashboard.menuLogout)).click();
	});
});