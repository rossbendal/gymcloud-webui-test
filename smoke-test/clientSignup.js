var EC = protractor.ExpectedConditions;
var elm;
var faker = require("faker");
var firstName = faker.Name.firstName();
var lastName = faker.Name.lastName();
var email = faker.Internet.email();
var randomName = faker.Name.findName();
var using = require('jasmine-data-provider');

function emailGenerator() {
	return(faker.Internet.email());
};

describe('Client Signup functionality', function() {

	//pre-testing, staging site needs auth
	browser.get("https://gymcloud:GCstaging@app.s.gymcloud.com/#login");
	element(by.css("[href='#login']"));
	elm = element(by.css(".btn[data-social='fb']"));
	browser.wait(EC.presenceOf(elm), 60000);

	using([{email:emailGenerator(),validCard:4242424242424242}, //Visa
		{email:emailGenerator(),validCard:4012888888881881},	//Visa
		{email:emailGenerator(),validCard:4000056655665556},	//Visa (debit)
		{email:emailGenerator(),validCard:5200828282828210},	//Mastercard
		{email:emailGenerator(),validCard:5105105105105100},	//Mastercard (debit)
		{email:emailGenerator(),validCard:378282246310005},		//American Express
		{email:emailGenerator(),validCard:6011111111111117},	//Discover
		{email:emailGenerator(),validCard:6011000990139424},	//Discover
		{email:emailGenerator(),validCard:30569309025904},		//Diners Club
		{email:emailGenerator(),validCard:38520000023237},		//Diners Club
		{email:emailGenerator(),validCard:3530111333300000},	//JCB
		{email:emailGenerator(),validCard:3566002020360505},	//JCB

		{email:emailGenerator(),validCard:4000000760000002},	//Brazil (BR) Visa
		{email:emailGenerator(),validCard:4000001240000000},	//Canada (CA) Visa
		{email:emailGenerator(),validCard:4000004840000008},	//Mexico (MX) Visa


		{email:emailGenerator(),validCard:4000004840000008},	//Austria (AT)	Visa
		{email:emailGenerator(),validCard:4000000560000004},	//Belgium (BE)	Visa
		{email:emailGenerator(),validCard:4000002080000001},	//Denmark (DK)	Visa
		{email:emailGenerator(),validCard:4000002460000001},	//Finland (FI)	Visa
		{email:emailGenerator(),validCard:4000002500000003},	//France (FR)	Visa
		{email:emailGenerator(),validCard:4000003720000005},	//Ireland (IE)	Visa
		{email:emailGenerator(),validCard:4000003800000008},	//Italy (IT)	Visa
		{email:emailGenerator(),validCard:4000004420000006},	//Luxembourg (LU)	Visa
		{email:emailGenerator(),validCard:4000005280000002},	//Netherlands (NL)	Visa
		{email:emailGenerator(),validCard:4000005780000007},	//Norway (NO)	Visa
		{email:emailGenerator(),validCard:4000006200000007},	//Portugal (PT)	Visa
		{email:emailGenerator(),validCard:4000006430000009},	//Russian Federation (RU)	Visa
		{email:emailGenerator(),validCard:4000007240000007},	//Spain (ES)	Visa
		{email:emailGenerator(),validCard:4000007520000008},	//Sweden (SE)	Visa
		{email:emailGenerator(),validCard:4000007560000009},	//Switzerland (CH)	Visa
		{email:emailGenerator(),validCard:4000008260000000},	//United Kingdom (GB)	Visa
		{email:emailGenerator(),validCard:4000058260000005},	//United Kingdom (GB)	Visa (debit)

		{email:emailGenerator(),validCard:4000000360000006},	//Australia (AU)	Visa
		{email:emailGenerator(),validCard:4000001560000002},	//China (CN)	Visa
		{email:emailGenerator(),validCard:4000003440000004},	//Hong Kong (HK)	Visa
		{email:emailGenerator(),validCard:4000003920000003},	//Japan (JP)	Visa
		{email:emailGenerator(),validCard:4000005540000008},	//New Zealand (NZ)	Visa
		{email:emailGenerator(),validCard:4000007020000003},	//Singapore (SG)	Visa











		],function(data){

		
		it('redirect user to client registration form', function() {
			browser.get("https://app.s.gymcloud.com/#signup");
			elm = element(by.css("h1.text-center"));
			browser.wait(EC.presenceOf(elm), 60000);
			expect(element(by.css("h1.text-center")).getText()).toEqual('Create a GymCloud Account');
		});

		it('populate form with valid credentials', function() {
			element(by.css("input[name='first_name']")).sendKeys(firstName);
			element(by.css("input[name='last_name']")).sendKeys(lastName);
			element(by.css("input[name='email']")).sendKeys(data.email);
			element(by.css("input[name='password']")).sendKeys("gymcloud");
			element(by.css("input[name='password_confirmation']")).sendKeys("gymcloud");
			element(by.css("input[name='agreement']")).click();

		});

		it('should redirect user to Training Plan Screen when client registration submit is clicked', function() {
			element(by.css("button[type='submit']")).click();
			elm = element(by.css(".plan .name div.popular"));
			browser.wait(EC.presenceOf(elm), 60000);
			expect(element(by.css("h1.text-center")).getText()).toEqual('Training Plan Select');
		});

		it('should select on Fitness Assessment and click next to payment page', function() {
			element(by.xpath("//input[@value='1_on_1_live_virtual_coaching']/following-sibling::div[@class='radio-button']")).click();
			element(by.css("[type='submit']")).click();
		});


		it('should enter payment information', function() {
			element(by.css(".number[type='text']")).sendKeys(data.validCard); //Mastercard
			element(by.css("select.exp_month")).element(by.css("[value='05']")).click();
			element(by.css("select.exp_year")).element(by.css("[value='2021']")).click();
			element(by.css("input.cvc[type='text']")).sendKeys("1234");
			element(by.css("input.full-name[type='text']")).clear().sendKeys(randomName);
			element(by.css("button[type='submit']")).click();
		});

		it('should redirect user to successful payment screen', function() {
			elm = element(by.css(".icon.row"));
			browser.wait(EC.presenceOf(elm), 60000);
			expect(element(by.css("h1.text-center")).getText()).toEqual('Payment Successful!');
		});

		it('restart the test for DDT.', function() {
			element(by.css("[href='#login']")).click();
			elm = element(by.css(".btn[data-social='fb']"));
			browser.wait(EC.presenceOf(elm), 60000);
		});
	});
});