var EC = protractor.ExpectedConditions;
var elm;
var faker = require("faker");
var firstName = faker.Name.firstName();
var lastName = faker.Name.lastName();
var email = faker.Internet.email();

describe('Pro registration', function() {
	//pre-testing, staging site needs auth
	browser.get("https://gymcloud:GCstaging@app.s.gymcloud.com/#login");
	browser.get("https://app.s.gymcloud.com/#login");
	element(by.css("[href='#login']"));
	elm = element(by.css(".btn[data-social='fb']"));
	browser.wait(EC.presenceOf(elm), 60000);

	it('should display `Sign Up` on login page', function() {
		elm = element(by.css("a[href='/#signup-role']"));
		browser.wait(EC.presenceOf(elm), 60000);
	});

	it('should redirect user to Account Type selection screen when Signup is clicked', function() {
		element(by.css("a[href='/#signup-role']")).click();
		elm = element(by.css(".gc-signup-form[role='form']"));
		browser.wait(EC.presenceOf(elm), 60000);
	});

	it('should redirect user to PRO form after clicking NEXT', function() {
		element(by.css("[type='submit']")).click();
		elm = element(by.css(".gc-login-social"));
		browser.wait(EC.presenceOf(elm), 60000);
	});

	it('User should be able to populate First Name', function() {
		element(by.css("#gc-signup-first-name")).clear().sendKeys(firstName+"-TEST");
	});

	it('User should be able to populate Last Name', function() {
		element(by.css("#gc-signup-last-name")).clear().sendKeys(lastName+"-TEST");
	});

	it('User should be able to populate Email Address', function() {
		element(by.css("#gc-signup-email")).clear().sendKeys(email+"TEST");
	});

	it('User should be able to populate Password field', function() {
		element(by.css("#gc-signup-password-1")).clear().sendKeys("gymcloud");
	});

	it('User should be able to populate Confirm-Password field', function() {
		element(by.css("#gc-signup-password-2")).clear().sendKeys("gymcloud");
	});

	it('User should be able to click EULA checkbox', function() {
		element(by.css(".gc-signup-agree")).click();
	});

	it('User should be able to enter signup', function() {
		element(by.css(".gc-signup[type='submit']")).click();
	});

	it('User should be able to be redirected to HomePage', function() {
		elm = element(by.css(".account-type-body.modal-body"));
		browser.wait(EC.presenceOf(elm), 80000);
		expect(element(by.css(".account-type-body.modal-body")).isPresent()).toBeTruthy();
		browser.executeScript('window.sessionStorage.clear();');
    	browser.executeScript('window.localStorage.clear();');
	});
});