var EC = protractor.ExpectedConditions;
var elm;
var faker = require("faker");
var firstName = faker.Name.firstName();
var lastName = faker.Name.lastName();
var email = faker.Internet.email();
var randomName = faker.Name.findName();
var using = require('jasmine-data-provider');

function emailGenerator() {
	return(faker.Internet.email());
};

describe('Client Signup functionality', function() {

	//pre-testing, staging site needs auth
	browser.get("https://gymcloud:GCstaging@app.s.gymcloud.com/#login");
	elm = element(by.css("button[type='submit']"));
	browser.wait(EC.presenceOf(elm), 60000);

	using([{sleep: 5000,validCard:4242424242424242}, //Visa
		{sleep:0,validCard:4012888888881881},	//Visa
		{sleep:0,validCard:4000056655665556},	//Visa (debit)
		{sleep:0,validCard:5200828282828210},	//Mastercard
		{sleep:0,validCard:5105105105105100},	//Mastercard (debit)
		{sleep:0,validCard:378282246310005},		//American Express
		{sleep:0,validCard:6011111111111117},	//Discover
		{sleep:0,validCard:6011000990139424},	//Discover
		{sleep:0,validCard:30569309025904},		//Diners Club
		{sleep:0,validCard:38520000023237},		//Diners Club
		{sleep:0,validCard:3530111333300000},	//JCB
		{sleep:0,validCard:3566002020360505},	//JCB

		{sleep:0,validCard:4000000760000002},	//Brazil (BR) Visa
		{sleep:0,validCard:4000001240000000},	//Canada (CA) Visa
		{sleep:0,validCard:4000004840000008},	//Mexico (MX) Visa


		{sleep:0,validCard:4000004840000008},	//Austria (AT)	Visa
		{sleep:0,validCard:4000000560000004},	//Belgium (BE)	Visa
		{sleep:0,validCard:4000002080000001},	//Denmark (DK)	Visa
		{sleep:0,validCard:4000002460000001},	//Finland (FI)	Visa
		{sleep:0,validCard:4000002500000003},	//France (FR)	Visa
		{sleep:0,validCard:4000003720000005},	//Ireland (IE)	Visa
		{sleep:0,validCard:4000003800000008},	//Italy (IT)	Visa
		{sleep:0,validCard:4000004420000006},	//Luxembourg (LU)	Visa
		{sleep:0,validCard:4000005280000002},	//Netherlands (NL)	Visa
		{sleep:0,validCard:4000005780000007},	//Norway (NO)	Visa
		{sleep:0,validCard:4000006200000007},	//Portugal (PT)	Visa
		{sleep:0,validCard:4000006430000009},	//Russian Federation (RU)	Visa
		{sleep:0,validCard:4000007240000007},	//Spain (ES)	Visa
		{sleep:0,validCard:4000007520000008},	//Sweden (SE)	Visa
		{sleep:0,validCard:4000007560000009},	//Switzerland (CH)	Visa
		{sleep:0,validCard:4000008260000000},	//United Kingdom (GB)	Visa
		{sleep:0,validCard:4000058260000005},	//United Kingdom (GB)	Visa (debit)

		{sleep:0,validCard:4000000360000006},	//Australia (AU)	Visa
		{sleep:0,validCard:4000001560000002},	//China (CN)	Visa
		{sleep:0,validCard:4000003440000004},	//Hong Kong (HK)	Visa
		{sleep:0,validCard:4000003920000003},	//Japan (JP)	Visa
		{sleep:0,validCard:4000005540000008},	//New Zealand (NZ)	Visa
		{sleep:0,validCard:4000007020000003},	//Singapore (SG)	Visa


		],function(data){

		
		it('pay using valid credit card numbers', function() {

			//Go to Sign Up
			browser.get("https://app.s.gymcloud.com/#signup");
			elm = element(by.css("h1.text-center"));
			browser.wait(EC.presenceOf(elm), 60000);
			expect(element(by.css("h1.text-center")).getText()).toEqual('Create a GymCloud Account');

			//Form Credentials
			element(by.css("input[name='first_name']")).sendKeys(firstName+"-TEST");
			element(by.css("input[name='last_name']")).sendKeys(lastName+"-TEST");
			element(by.css("input[name='email']")).sendKeys(emailGenerator());
			element(by.css("input[name='password']")).sendKeys("gymcloud");
			element(by.css("input[name='password_confirmation']")).sendKeys("gymcloud");
			element(by.css("input[name='agreement']")).click();
			element(by.css("button[type='submit']")).click();

			//Training Plan Redirect
			elm = element(by.css("div.popular_whmDR"));
			browser.wait(EC.presenceOf(elm), 60000);
			expect(element(by.css("h1 span")).getText()).toEqual('Select Training Plan');

			//Select Fitness Plan
			//element(by.xpath("/x:html/x:body/x:region[1]/x:div/x:div/x:region/x:header/x:div/x:div/x:form/x:div[1]/x:label[2]/x:div[2]/x:div[2]")).click();
			element(by.css("[type='submit']")).click();
			
			//Enter payment info
			element(by.css(".number[type='text']")).sendKeys(data.validCard); //Mastercard
			element(by.css("select.exp_month")).element(by.css("[value='05']")).click();
			element(by.css("select.exp_year")).element(by.css("[value='2021']")).click();
			element(by.css("input.cvc[type='text']")).sendKeys("1234");
			element(by.css("input.full-name[type='text']")).clear().sendKeys(randomName);
			elm = element(by.css("[type='submit']"));
			browser.wait(EC.presenceOf(elm), 60000);
			browser.sleep(data.sleep);
			element(by.css("button[type='submit']")).click();

			

			//Redirect to successful screen
			elm = element(by.css(".icon_6onz-.row"));
			browser.wait(EC.presenceOf(elm), 60000);
			expect(element(by.css("h1 span")).getText()).toEqual('Payment Successful!');

			//DDT Teardown
			browser.executeScript('window.sessionStorage.clear();');
    		browser.executeScript('window.localStorage.clear();');

		});

	},120000); //120 seconds timeout for this spec

});




