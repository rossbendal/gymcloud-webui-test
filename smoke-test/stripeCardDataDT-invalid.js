var EC = protractor.ExpectedConditions;
var elm;
var faker = require("faker");
var firstName = faker.Name.firstName();
var lastName = faker.Name.lastName();
var email = faker.Internet.email();
var randomName = faker.Name.findName();
var using = require('jasmine-data-provider');

function emailGenerator() {
	return(faker.Internet.email());
};

describe('Attempt to pay using invalid card', function() {

	//pre-testing, staging site needs auth
	browser.get("https://gymcloud:GCstaging@app.s.gymcloud.com/#login");
	elm = element(by.css("button[type='submit']"));
	browser.wait(EC.presenceOf(elm), 60000);

	using([{sleep:15000,validCard:4242424242424241}, //Charge is declined with an incorrect_number code as the card number fails the Luhn check.
		{sleep:0,validCard:4000000000000119},	//Charge is declined with a processing_error code
		// {sleep:0,validCard:4000000000000069},	//Charge is declined with an expired_card code.
		{sleep:0,validCard:4000000000000127},	//Charge is declined with an incorrect_cvc code.
		{sleep:0,validCard:4100000000000019},	//Charge is declined with a card_declined code and a fraudulent reason.
		{sleep:0,validCard:4000000000000002},	//Charge is declined with a card_declined code.


		],function(data){

		
		it('pay using valid credit card numbers', function() {

			//Go to Sign Up
			browser.get("https://app.s.gymcloud.com/#signup");
			elm = element(by.css("h1.text-center"));
			browser.wait(EC.presenceOf(elm), 60000);
			expect(element(by.css("h1.text-center")).getText()).toEqual('Create a GymCloud Account');

			//Form Credentials
			element(by.css("input[name='first_name']")).sendKeys(firstName);
			element(by.css("input[name='last_name']")).sendKeys(lastName);
			element(by.css("input[name='email']")).sendKeys(emailGenerator());
			element(by.css("input[name='password']")).sendKeys("gymcloud");
			element(by.css("input[name='password_confirmation']")).sendKeys("gymcloud");
			element(by.css("input[name='agreement']")).click();

			// //Training Plan Redirect
			element(by.css("button[type='submit']")).click();
			elm = element(by.css("div.popular_whmDR"));
			browser.wait(EC.presenceOf(elm), 60000);
			expect(element(by.css("h1 span")).getText()).toEqual('Select Training Plan');

			// //Select Fitness Plan
			// element(by.xpath("//input[@value='personal_coaching']/following-sibling::div[@class='radio-button']")).click();
			element(by.css("[type='submit']")).click();

			// //Enter payment info
			element(by.css(".number[type='text']")).sendKeys(data.validCard); //Mastercard
			element(by.css("select.exp_month")).element(by.css("[value='05']")).click();
			element(by.css("select.exp_year")).element(by.css("[value='2021']")).click();
			element(by.css("input.cvc[type='text']")).sendKeys("1234");
			element(by.css("input.full-name[type='text']")).clear().sendKeys(randomName);
			elm = element(by.css("button[type='submit']"));
			browser.wait(EC.presenceOf(elm), 60000);
			browser.sleep(data.sleep);
			element(by.css("button[type='submit']")).click();

			//Display error prompt
			elm = element(by.css(".messenger-shown p"));
			browser.wait(EC.presenceOf(elm), 60000);
			expect(element(by.css(".messenger-shown p")).isPresent()).toBeTruthy();

			//DDT Teardown
			browser.executeScript('window.sessionStorage.clear();');
    		browser.executeScript('window.localStorage.clear();');

		});

	});

});